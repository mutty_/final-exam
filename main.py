### Final Exam Program                 ###
### By Matthew Nocera-Iozzo            ###
### PROG10004 - Programming Principles ###
### For Professor Mohamed              ###

from student import *
import csv
import time


def summary():
    with open("input_file.csv", "r") as f:
        # read CSV
        csvreader = csv.reader(f)
        # create empty list named students
        students = []
        # read content of file
        lineCount = 1
        for row in csvreader:
            if lineCount > 1:
                id = row[0]
                name = row[1]
                midterm = row[2]
                final = row[3]
                # create student object
                s = Student(id, name, midterm, final)
                # append obj to students list
                students.append(s)
            lineCount += 1
        # print(students)
        # print(students[1].id)

    # generate output_file.csv
    with open("output_file.csv", "w", newline='') as f:
        csvwriter = csv.writer(f)
        csvwriter.writerow(["id", "name", "total", "status"])

        for i, item in enumerate(students):
            csvwriter.writerow(
                [students[i].id, students[i].name, students[i].total(), students[i].status()])

    with open("output_file.csv", "r") as f:
        csvreader = csv.reader(f)
        # no. of students = csv lines - 1 (header)
        classSize = len(list(f)) - 1

    with open("output_file.csv", "r") as f:
        csvreader = csv.reader(f)
        # class average
        lineCount = 1
        classAvg = 0
        for row in csvreader:
            if lineCount > 1:
                classAvg += (int(float(row[2])))
            lineCount += 1
        classAvg = (int(classAvg) / classSize)

    with open("output_file.csv", "r") as f:
        # find highest-graded student
        csvreader = csv.reader(f)
        lineCount = 1
        maxGrades = []
        maxStudent = []
        for row in csvreader:
            if lineCount > 1:
                maxStudent.append(row[1])
                maxGrades.append(row[2])
            lineCount += 1
        highestGrade = max(maxGrades)
        highestStudent = maxStudent[maxGrades.index(highestGrade)]

    print(f"There are {classSize} students in this class.")
    print(f"The class average is {round(classAvg, 1)}%.")
    print(
        f"The highest-scoring student is {highestStudent} with {highestGrade}%.")
    print(f"Please see output_file.csv for more details.")


print("Welcome to the Sheridan Automatic Grading System.")
time.sleep(2)
summary()
