class Student():
    def __init__(self, id, name, midterm, final):
        self.id = id
        self.name = name
        self.midterm = midterm
        self.final = final

    def total(self):
        return float(self.midterm) + float(self.final)

    def status(self):
        if self.total() > 50:
            return "Pass"
        else:
            return "Fail"